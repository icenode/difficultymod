  "engineer" SPELL
  {
	"build turret" 2
  }
  "flame" SPELL
  {
	"fireball" 2
	"firewall" 4
	"fire breath" 8
	"meteor shower" 12
	"fire elemental" 5
	"advanced fire elemental" 10
  }
  "necromancer" SPELL
  {
	"summon skeletons" 2
	"summon vampire bodyguards" 3
	"raise zombies" 4
	"advanced summon skeletons" 6
	"summon vampires" 9
	"master summon skeletons" 10
	"summon vampire horde" 12
  }
  "necromancer lesser" SPELL
  {
	"summon skeletons" 2
	"summon vampire bodyguards" 3
	"raise zombies" 4
  }
  
  "druid" SPELL
  {
	"advanced healing" 1
    "expert healing" 2
    "cure poison" 3
	"cure blindness" 5
    "poison resistance" 6
	"magic missile" 3
	"escape" 5
	"slowing vines" 2
	"call on nature" 8
	"call on poison aspect" 11
	"ice cone" 10
	"tranquility" 12
  }
  "dirtytricks" MELEE
  {
	"flash powder" 2
	"throw knife" 4
	"desperate technique" 5
  }
